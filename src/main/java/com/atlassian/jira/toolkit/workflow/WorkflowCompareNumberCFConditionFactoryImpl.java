package com.atlassian.jira.toolkit.workflow;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.customfields.impl.NumberCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;
import com.atlassian.jira.util.collect.MapBuilder;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Factory for editing plugins that require a Number custom field.
 */
public class WorkflowCompareNumberCFConditionFactoryImpl extends AbstractWorkflowPluginFactory implements WorkflowPluginConditionFactory {
    private final FieldManager fieldManager;


    public WorkflowCompareNumberCFConditionFactoryImpl(FieldManager fieldManager) {
        this.fieldManager = fieldManager;
    }

    protected void getVelocityParamsForInput(Map velocityParams) {
        List fields = getNumberCustomFields();
        Map numberCFs = new HashMap(fields.size());
        Iterator iter = fields.iterator();
        while (iter.hasNext()) {
            CustomField customField = (CustomField) iter.next();
            numberCFs.put(customField.getId(), customField.getName());

        }
        velocityParams.put("numbercfs", numberCFs);
        velocityParams.put("comparators", MapBuilder.build(CompareNumberCFCondition.EQUALS, "equals",
                CompareNumberCFCondition.LESS_THAN, "is less than",
                CompareNumberCFCondition.GREATER_THAN, "is greater than"));

    }

    public List /* <Field> */ getNumberCustomFields() {
        List fields = new ArrayList();

        Set fieldSet;
        try {
            fieldSet = fieldManager.getAllAvailableNavigableFields();
        } catch (FieldException e) {
            return Collections.EMPTY_LIST;
        }

        for (Iterator i = fieldSet.iterator(); i.hasNext(); ) {
            Field field = (Field) i.next();
            if (fieldManager.isCustomField(field)) {
                CustomField customField = (CustomField) field;
                // Exclude field types that obviously don't specify a group
                if (customField.getCustomFieldType() instanceof NumberCFType) {
                    fields.add(field);
                }
            }
        }

        return fields;

    }

    protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor) {
        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);
    }

    protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor) {
        if (!(descriptor instanceof ConditionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ConditionDescriptor.");
        }

        ConditionDescriptor conditionDescriptor = (ConditionDescriptor) descriptor;

        FieldManager fieldManager = ComponentAccessor.getFieldManager();
        Map args = conditionDescriptor.getArgs();
        String fieldId = (String) args.get("numbercf");
        Field field = fieldManager.getField(fieldId);
        velocityParams.put("numbercfName", (field == null ? fieldId : field.getName()));
        String comparator = (String) args.get("comparator");
        velocityParams.put("comparator", comparator);
        String value = (String) args.get("value");
        velocityParams.put("value", value);

    }

    public Map getDescriptorParams(Map conditionParams) {
        // Process The map
        String value = extractSingleParam(conditionParams, "numbercf");
        Map map = new HashMap();
        map.put("numbercf", value);
        value = extractSingleParam(conditionParams, "comparator");
        map.put("comparator", value);
        value = extractSingleParam(conditionParams, "value");
        map.put("value", value);

        return map;
    }
}
