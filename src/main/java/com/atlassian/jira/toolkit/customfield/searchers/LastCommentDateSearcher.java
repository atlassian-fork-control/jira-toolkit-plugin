package com.atlassian.jira.toolkit.customfield.searchers;

import java.util.Date;

import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.SortableCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.searchers.CustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.DateTimeRangeSearcher;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.search.searchers.information.SearcherInformation;
import com.atlassian.jira.issue.search.searchers.renderer.SearchRenderer;
import com.atlassian.jira.issue.search.searchers.transformer.SearchInputTransformer;
import com.atlassian.jira.issue.statistics.DateFieldSorter;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptor;
import com.atlassian.jira.util.JiraUtils;

/**
 * Delegates to the DateTimeRangeSearcher from JIRA core.
 *
 * @since v0.26
 */
public class LastCommentDateSearcher implements SortableCustomFieldSearcher, CustomFieldSearcher {
    private final DateTimeRangeSearcher delegate;

    public LastCommentDateSearcher() {
        // Get Pico to inject DateTimeRangeSearcher with dependencies
        this.delegate = JiraUtils.loadComponent(DateTimeRangeSearcher.class);
    }

    @Override
    public void init(final CustomField field) {
        delegate.init(field);
    }

    @Override
    public SearcherInformation<CustomField> getSearchInformation() {
        return delegate.getSearchInformation();
    }

    @Override
    public SearchInputTransformer getSearchInputTransformer() {
        return delegate.getSearchInputTransformer();
    }

    @Override
    public SearchRenderer getSearchRenderer() {
        return delegate.getSearchRenderer();
    }

    @Override
    public CustomFieldSearcherClauseHandler getCustomFieldSearcherClauseHandler() {
        return delegate.getCustomFieldSearcherClauseHandler();
    }

    @Override
    public LuceneFieldSorter<Date> getSorter(CustomField customField) {
        return new DateFieldSorter(customField.getId());
    }

    @Override
    public CustomFieldSearcherModuleDescriptor getDescriptor() {
        return delegate.getDescriptor();
    }

    @Override
    public void init(final CustomFieldSearcherModuleDescriptor customFieldSearcherModuleDescriptor) {
        delegate.init(customFieldSearcherModuleDescriptor);
    }
}
