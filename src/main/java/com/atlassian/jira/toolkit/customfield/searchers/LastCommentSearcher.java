package com.atlassian.jira.toolkit.customfield.searchers;

import com.atlassian.jira.issue.customfields.searchers.ExactTextSearcher;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.web.FieldVisibilityManager;

/**
 * @since JIRA 4.0
 */
public class LastCommentSearcher extends ExactTextSearcher {

    public LastCommentSearcher(final JqlOperandResolver jqlOperandResolver, final CustomFieldInputHelper customFieldInputHelper, final FieldVisibilityManager fieldVisibilityManager) {
        super(jqlOperandResolver, customFieldInputHelper, fieldVisibilityManager);
    }
}
