package com.atlassian.jira.toolkit.customfield.searchers;

import com.atlassian.jira.issue.customfields.statistics.AbstractCustomFieldStatisticsMapper;
import com.atlassian.jira.issue.fields.CustomField;

/**
 * This code was copied from JIRA core, as TextStatisticsMapper was being ripped out of there (it lived in the test source tree!).
 *
 * @since JIRA v4.0
 */
public class TextStatisticsMapper extends AbstractCustomFieldStatisticsMapper {
    public TextStatisticsMapper(CustomField customField) {
        super(customField);
    }

    public Object getValueFromLuceneField(String documentValue) {
        return documentValue;
    }

    protected String getSearchValue(Object value) {
        return value != null ? value.toString() : "";
    }
}

