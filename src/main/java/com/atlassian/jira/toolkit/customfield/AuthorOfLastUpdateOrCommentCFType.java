/**
 *
 */
package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.comparator.UserComparator;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Get the author of either the last update or the last comment, whatever came latest.
 *
 * @author Paul Curren
 */
public class AuthorOfLastUpdateOrCommentCFType extends CalculatedCFType<ApplicationUser, ApplicationUser> implements SortableCustomField<ApplicationUser>, UserField {

    private static final Logger log = Logger.getLogger(AuthorOfLastUpdateOrCommentCFType.class);
    private static final UserComparator USER_COMPARATOR = new UserComparator();

    private final CommentManager commentManager;
    private final UserConverter userConverter;
    private final ChangeHistoryManager changeHistoryManager;
    private final WorklogManager worklogManager;

    public AuthorOfLastUpdateOrCommentCFType(CommentManager commentManager, UserConverter userConverter,
                                             ChangeHistoryManager changeHistoryManager, WorklogManager worklogManager) {
        this.commentManager = commentManager;
        this.userConverter = userConverter;
        this.changeHistoryManager = changeHistoryManager;
        this.worklogManager = worklogManager;
    }

    @Override
    public ApplicationUser getSingularObjectFromString(String username) throws FieldValidationException {
        // get user from user name (matches what return from getStringFromSingularObject)
        return userConverter.getUserFromHttpParameterWithValidation(username);
    }

    @Override
    public String getStringFromSingularObject(ApplicationUser obj) {
        // return user name
        return userConverter.getHttpParameterValue(obj);
    }

    /**
     * @return a String representing the user name of the last updater or commenter.
     */
    @Override
    public ApplicationUser getValueFromIssue(CustomField customfield, Issue issue) {
        String updaterKey = issue.getReporterId(); //key
        Date updateTime = issue.getCreated();

        final List changeHistories = changeHistoryManager.getChangeHistoriesForUser(issue, null);
        if (!changeHistories.isEmpty()) {
            ChangeHistory latestChangeHistory = (ChangeHistory) changeHistories.get(changeHistories.size() - 1);
            updaterKey = latestChangeHistory.getAuthor(); //key
            updateTime = latestChangeHistory.getTimePerformed();
        }

        // we now have the latest change. Check the dates of each comment against the last update time.
        final List comments = commentManager.getComments(issue);
        for (Iterator it = comments.iterator(); it.hasNext(); ) {
            Comment comment = (Comment) it.next();
            if (comment.getCreated().after(updateTime)) {
                updaterKey = comment.getAuthor(); //key
                updateTime = comment.getCreated();
            }
        }

        //now check that if there's any later worklogs
        final List worklogs = worklogManager.getByIssue(issue);
        if (!worklogs.isEmpty()) {
            Worklog latestWorklog = (Worklog) worklogs.get(worklogs.size() - 1);
            if (latestWorklog.getUpdated().after(updateTime)) {
                updaterKey = latestWorklog.getUpdateAuthor(); //key
            }
        }

        try {
            // use the user key to lookup the value and return
            return userConverter.getUserFromDbString(updaterKey);
        } catch (FieldValidationException e) {
            log.warn("Userkey '" + updaterKey +
                    "' could not be found. The user has probably been deleted and was the last user to update issue '"
                    + issue.getKey() + "'.");
        }
        return null;
    }

    @Override
    public int compare(@Nonnull ApplicationUser o1, @Nonnull ApplicationUser o2, FieldConfig fieldConfig) {
        return USER_COMPARATOR.compare((ApplicationUser) o1, (ApplicationUser) o2);
    }
}
